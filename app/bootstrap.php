<?php

// Inicio de session
if( !session_id() ) @session_start();

require_once 'vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

spl_autoload_register(function ($className) {
    require_once 'libraries/' . $className . '.php';
});


// carga Paginas.php 

require_once '../app/controllers/Paginas.php';

// carga config.php

require_once '../app/config/config.php';

// carga utils.php

require_once '../app/helpers/utils.php';
