<?php require_once APPROOT . '/views/partials/header.php'; ?>
<?php require_once APPROOT . '/views/partials/navbar.php'; ?>

    <div class="container container-incidencia">
        <div class="row">
            <div class="col-md-6">
                
                
                <h2><?= $data['profile']->name ?></h2>
                <p class="text-white"><span class="badge rounded-pill <?= $data['profile']->rol === 'visitor' ? 'bg-success' : ($data['profile']->rol === 'user' ? 'bg-danger' : 'bg-dark') ?> ml-2"><?= $data['profile']->rol === 'visitor' ? 'Visitante' : ($data['profile']->rol === 'user' ? 'Usuario' : 'Admin') ?></span></p>
                <hr>
                <p><b>email:</b></p>
                <h3><?= $data['profile']->email?></h3>
                <hr>
                <p><b>Usuario del SCI desde:</b></p>
                <h4><?= $data['profile']->created_at ?></h4>
                <hr>
                <a href="<?= URLROOT.'/incidencias/index'?>" class="btn btn-warning"><i class="fas fa-arrow-left"></i>

                    Volver</a>

                <?php if($_SESSION['user_rol'] === 'admin' && $_SESSION['user_id'] !== $data['profile']->id ): ?>
                <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#exampleModal"><i class="fas fa-trash"></i>
                     Eliminar
                </button>
                <?php endif; ?>

            </div>
            <div class="col-md-6 d-flex flex-column align-items-center justify-content-center">
                <img class="avatar avatar-128 bg-light rounded-circle text-white p-2"
                    src="<?= !empty($data['profile']->image) ? URLROOT.'/public/images/profiles/'.$data['profile']->image : 'https://raw.githubusercontent.com/twbs/icons/main/icons/person-fill.svg'?>">
                    
                    <?php if($_SESSION['user_rol'] === 'admin' ): ?>
                    
                    <p>Cambiar rol de usuario:</p>
                    <form method="post" action="<?= URLROOT.'/profiles/rol/'.$data['profile']->id ?>">
                        <select name="rol" class="form-select mt-3" aria-label="Default select example">
                            <option value="visitor">Visitante</option>
                            <option value="user">User</option>
                            <option value="admin">Admin</option>
                         </select>
                        <button type="submit" class="btn btn-success mt-3">Cambiar</button>
                    </form>
                    <?php endif; ?>
            </div>          
        </div>
            
    </div>
            <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">¿Eliminar?</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Esta acción es irreversible. ¿Está seguro de que desea eliminar al usuario?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                <form action="<?= URLROOT.'/profiles/delete/'.$data['profile']->id ?>" method="post">                        
                        <button type="submit" class="btn btn-danger">
                            <i class="fas fa-trash"></i> Confirmar eliminación
                        </button>
                </form>
            </div>
            </div>
        </div>
    </div>


<?php require_once APPROOT . '/views/partials/footer.php'; ?>