<?php require_once APPROOT . '/views/partials/header.php'; ?>
<?php require_once APPROOT . '/views/partials/navbar.php'; ?>

    <div class="container container-incidencia">
        <div class="row">
            <div class="col-md-6">
            <h2>Editar Perfil</h2>
                <form action="<?= URLROOT.'/profiles/edit/'.isset($data['profile']->id) ? $data['profile']->id : $data['id'] ?>" enctype="multipart/form-data" method="post">
                    <h5>Nombre:</h5>
                    <input type="text" name="name" value="<?= isset($data['name']) ? $data['name'] : $data['profile']->name ?>" class="form-control form-control-sm <?= (!empty($data['name_err'])) ? 'is-invalid' : '' ?>">
                    <span class="invalid-feedback"><?= $data['name_err'] ?></span>
                    <hr>
                    <h5>Email:</h5>
                    <input type="text" name="email" value="<?= isset($data['email']) ? $data['email'] : $data['profile']->email?>" class="form-control form-control-sm <?= (!empty($data['email_err'])) ? 'is-invalid' : '' ?>">
                    <span class="invalid-feedback"><?= $data['email_err'] ?></span>
                    <hr>
                    <h5>Avatar:</h5>
                    <input type="file" name="image" class="form-control form-control-sm <?= (!empty($data['image_err'])) ? 'is-invalid' : '' ?>">
                    <span class="invalid-feedback"><?= $data['image_err'] ?></span>
                    <hr>
                    <input type="submit" value="Actualizar" class="btn btn-success btn-block mt-3">
                    
                </form>    
                    <a href="<?= URLROOT.'/incidencias/index' ?>" class="btn btn-warning btn-block mt-3"><i class="fas fa-arrow-left"></i>Volver</a>
            </div>
            <div class="col-md-6 d-flex flex-column align-items-center justify-content-center">
                <img class="avatar avatar-128 bg-light rounded-circle text-white p-2"
                    src="<?= !empty($data['profile']->image) ? URLROOT.'/public/images/profiles/'.$data['profile']->image : 'https://raw.githubusercontent.com/twbs/icons/main/icons/person-fill.svg'?>">
            </div>          
        </div>
            
    </div>


<?php require_once APPROOT . '/views/partials/footer.php'; ?>