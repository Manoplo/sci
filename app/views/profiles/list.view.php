<?php require_once APPROOT . '/views/partials/header.php'; ?>
<?php require_once APPROOT . '/views/partials/navbar.php'; ?>
        <div class="container container-incidencia">
            <div class="row">
                <div class="col-md-12">
                <div class="flashes">
                    <?= (string) flash() ?>
                </div>
                <h1>Gestión de usuarios</h1>
                <div class="row">
                    <div class="col-md-12 d-flex">
                    <form method="post" action="<?= URLROOT.'/profiles/search'?>" class="d-flex flex-direction-row mt-3 mb-3">
                        <input class="form-control me-2" type="search" name="input" placeholder="Introduce un nombre..." aria-label="Search">
                        <button class="btn btn-success d-flex align-items-center" type="submit"><i class="fas fa-search"></i>Buscar</button>
                    </form>
                    
                    </div>
                </div>
                <p class="text-danger"><?= isset($data['search_err']) ? $data['search_err'] : ''; ?></p>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nombre</th>
                                <th>Avatar</th>
                                <th>email</th>
                                <th>Rol</th>
                                <th>Fecha de registro</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($data['profiles'] as $profile): ?>
                                <tr>
                                    <td><?= $profile->id ?></td>
                                    <td><?= $profile->name ?></td>
                                    <td><img class="avatar avatar-32 bg-light rounded-circle text-white p-2" src="<?= !empty($profile->image) ? URLROOT.'/public/images/profiles/'.$profile->image : 'https://raw.githubusercontent.com/twbs/icons/main/icons/person-fill.svg'?>" alt="Imagen de la profile" width="50px"></td>
                                    <td><?= $profile->email ?></td>
                                    <td><?= $profile->rol ?></td>
                                    <td><?= $profile->created_at ?></td>
                                    <td>
                                    <a href="<?= URLROOT.'/profiles/show/'.$profile->id ?>" class="btn btn-warning btn-sm d-flex align-items-between"><span class="material-icons">
                                        search
                                        </span>Ver</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            
                        </tbody>
                    </table>
            <a href="<?= URLROOT.'/incidencias/index/'?>" class="btn btn-warning"><i class="fas fa-arrow-left"></i>Volver</a>
        </div>

                </div>
            </div>
        </div>
        <!--- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">¿Eliminar?</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Esta acción es irreversible. ¿Está seguro de que desea eliminar todas las incidencias resueltas?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                <form action="<?= URLROOT.'/incidencias/destroy/'?>" method="post">                        
                        <button type="submit" class="btn btn-danger">
                        <i class="fas fa-recycle"></i> Confirmar eliminación
                        </button>
                </form>
            </div>
            </div>
        </div>
    </div>

        
<?php require_once APPROOT . '/views/partials/footer.php'; ?>
