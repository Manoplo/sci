<?php require_once APPROOT . '/views/partials/header.php'; ?>
<?php require_once APPROOT . '/views/partials/navbar.php'; ?>
        

        <div class="container robot-container">
            <div class="row">
            
                            <div class="flashes">
                                <?= (string) flash() ?>
                            </div>
                    <h1>AI Train Center</h1>
                    <a href="<?= URLROOT ?>/chatbot/chat"><button  class="btn btn-warning mt-1 mb-4"><i class="fas fa-arrow-left"></i>Salir del entrenamiento</button></a>
                    
                    <?php foreach($data['nullAnswers'] as $nullAnswer) : ?> 
                        <div class="col-md-3">
                            <div class="train-container mb-4">
                                <h5 class="text-primary">Pregunta:</h5>
                                <p><?= $nullAnswer->question; ?></p> 
                                <form method="post" action="<?= URLROOT.'/chatbot/learn/'.$nullAnswer->id ?>" class="row g-2">
                                    <div class="col-auto">
                                        <input type="text" class="form-control" name="answer" placeholder="Enseñame la respuesta...">
                                    </div>
                                    <div class="col-auto">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>   
                            </div>    
                        </div> 
                    <?php endforeach; ?>
                </div>
            </div>

<?php require_once APPROOT . '/views/partials/footer.php'; ?>