<?php require_once APPROOT . '/views/partials/header.php'; ?>
<?php require_once APPROOT . '/views/partials/navbar.php'; ?>


<div class="container robot-container">
    <div class="row">
        <div class="col-md-6">
            <h1>¡Hola! <span class="emoji">👋</span> </h1>
            <h1>Soy <span class="text-primary">Incidencias-Bot</span> <img src="<?=URLROOT.'/public/images/robot2.gif'?>" width="180px" style="margin-left: -20px" alt=""> </h1>
            <ul>
                <li><p>Procura escribir preguntas concretas. Sólo soy un robot.</p></li>
                <li><p>¡Puedes preguntarme cosas, pero no lo sé todo!</p> </li>
                <li><p>Me hago más inteligente con tus preguntas.</p> </li>
            </ul>
            <a class="btn btn-primary mb-3 mt-3" href="<?= URLROOT.'/incidencias/index' ?>"><i class="fas fa-arrow-left"></i> Volver</a>

            <?php if($_SESSION['user_rol'] === 'admin'): ?>
            <a class="btn btn-warning mb-3 mt-3" href="<?= URLROOT.'/chatbot/train' ?>"><i class="fas fa-brain"></i>Entrenar</a>
            <?php endif; ?>

        </div>
        <div class="col-md-6">
            <div class="chat-container">
                <!--- SCREEN -->
                <div class="screen">

                    <div class="question-container">
                    <div class="bot">
                            <div class="from-user">
                                <p class="from-user-name text-info"><?= $_SESSION['user_name']?></p>
                                <p class="text-white"><?= $data['question'] ?></p>
                            </div>     
                        </div>
                    </div>
                    
                    <div class="answer-container">
                        <div class="bot">
                            <p class="from-me text-white"><?= !empty($data['answer']) ? $data['answer']->answer : (isset($data['no_answer']) ? $data['no_answer'] : $data['waiting'])  ?></p>
                            <img src="<?= URLROOT.'/public/images/robot2.gif'?>" width="150px" alt="Bot">  
                        </div>
                    </div>
                </div>
                <!--- SCREEN -->
                <div class="input-container">
                        <form class="row g-2" method="post" action="<?= URLROOT.'/chatbot/chat'?>">
                            <div class="col-md-10">
                                <input type="text" name="question" class="form-control" id="input" placeholder="Escribe tu pregunta">     
                            </div>
                            <div class="col-auto">
                                <button type="submit" class="btn btn-primary my-auto" id="send">Enviar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php require_once APPROOT . '/views/partials/footer.php'; ?>