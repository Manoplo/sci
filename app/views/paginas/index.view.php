<?php require_once APPROOT . '/views/partials/header.php'; ?>
<div class="main-wrapper">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-md-6">
                <div class="index-container">
                    <img src="<?php echo URLROOT; ?>/public/images/logo-cut.png" alt="Logo" width="70%" class="img-fluid mb-4">
                    <h1 class="text-center">
                        <?php echo $data['titulo']; ?> 
                    </h1>
                    <p>Versión: <span class="badge bg-success"><?= APPVERSION ?></span></p> 
                        
                        <a class="btn btn-block btn-info mt-3 w-100" href="<?= URLROOT.'/users/login'?>" role="button">Log-in</a>
                       
                        <a class="btn btn-warning mt-3  mb-3 w-100" href="<?= URLROOT.'/users/register'?>" role="button">Regístrate</a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once APPROOT . '/views/partials/footer.php'; ?>




<?php require_once APPROOT . '/views/partials/footer.php'; ?>