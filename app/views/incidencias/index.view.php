<?php require_once APPROOT . '/views/partials/header.php'; ?>
<?php require_once APPROOT . '/views/partials/navbar.php'; ?>

    <div class="container main-container">
        <div class="row">
            <div class="col-md-12">
                <div class="flashes">
                    <?= (string) flash() ?>
                </div>
                <h2>Bienvenid@ al Sistema de Control de Incidencias del CIFP Majada Marcial.</h2>
                <div class="row">
                    <?php if( $_SESSION['user_rol'] === 'admin' || $_SESSION['user_rol'] === 'user' ): ?>
                    <div class="col-md-4">
                        <a href="<?= URLROOT.'/incidencias/create'?>">
                        <div class="card-options">
                            <img src="<?= URLROOT.'/public/images/undraw_add_notes_re_ln36.svg'?>" width="100px" alt="">
                            <h2>Añadir Incidencia</h2>
                        </div>
                        </a>
                    </div>
                    <?php endif; ?>


                    <div class="col-md-4">
                        <a href="<?= URLROOT.'/incidencias/list'?>"> 
                            <div class="card-options-blue">
                                <img src="<?= URLROOT.'/public/images/undraw_publish_post_re_wmql.svg'?>" width="120px" alt="">
                                <h2><?= $_SESSION['user_rol'] === 'admin' ? 'Gestionar Incidencias' : 'Filtrar Incidencias' ?></h2>
                            </div>
                        </a>
                    </div>

                    <?php if( $_SESSION['user_rol'] === 'admin' ): ?>
                    <div class="col-md-4">
                        <a href="<?= URLROOT.'/profiles/list'?>">
                        <div class="card-options-green">
                            <img src="<?= URLROOT.'/public/images/undraw_online_cv_re_gn0a.svg'?>" width="100px" alt="">
                            <h2>Gestionar Usuarios</h2>
                        </div>
                        </a>
                    </div>
                    <?php endif; ?>
                </div>
            </div>      
        </div>
        <div class="row">
            <div class="col-md-12 mt-5">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Sin resolver</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Resueltas</button>
                </li>
            </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <div class="table-responsive">

                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Vista Previa</th>
                                                <th>Descripción</th>
                                                <th>Aula</th>
                                                <th>Fecha Creación</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($data['incidenciasU'] as $incidenciaU): ?>
                                                <tr>
                                                    <td><?= $incidenciaU->id ?></td>
                                                    <td><img src="<?= !empty($incidenciaU->image) ? URLROOT.'/public/images/'.$incidenciaU->image : URLROOT.'/public/images/default.jpg'?>" alt="Imagen de la incidencia" width="50px"></td>
                                                    <td><?= $incidenciaU->description ?></td>
                                                    <td><?= $incidenciaU->class ?></td>
                                                    <td><?= $incidenciaU->created_at ?></td>
                                                    <td>
                                                    <a href="<?= URLROOT.'/incidencias/show/'.$incidenciaU->id ?>" class="btn btn-warning btn-sm d-flex align-items-between"><span class="material-icons">
                                                        search
                                                        </span>Ver</a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="row mt-3">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Vista Previa</th>
                                                <th>Descripción</th>
                                                <th>Aula</th>
                                                <th>Fecha Resolución</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach($data['incidenciasS'] as $incidenciaS): ?>
                                                <tr>
                                                    <td><?= $incidenciaS->id ?></td>
                                                    <td><img src="<?= !empty($incidenciaS->image) ? URLROOT.'/public/images/'.$incidenciaS->image : URLROOT.'/public/images/default.jpg'?>" alt="Imagen de la incidencia" width="50px"></td>
                                                    <td><?= $incidenciaS->description ?></td>
                                                    <td><?= $incidenciaS->class ?></td>
                                                    <td><?= $incidenciaS->updated_at ?></td>
                                                    <td>
                                                    <a href="<?= URLROOT.'/incidencias/show/'.$incidenciaS->id ?>" class="btn btn-warning btn-sm d-flex align-items-between"><span class="material-icons">
                                                        search
                                                        </span>Ver</a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 d-flex justify-content-end">
                <div class="bot">
                    <p class="from-me">¡Hola! ¿Tienes alguna duda? ¡Vamos a hablar!</p>
                        <a href="<?= URLROOT.'/chatbot/chat'?>">
                            <img src="<?= URLROOT.'/public/images/robot2.gif'?>" width="150px" alt="Bot">  
                        </a>
                </div>
            </div>
        </div>
        
    </div>





<?php require_once APPROOT . '/views/partials/footer.php'; ?>