<?php require_once APPROOT . '/views/partials/header.php'; ?>
<?php require_once APPROOT . '/views/partials/navbar.php'; ?>

    <div class="container container-incidencia">
        <div class="row">
            <div class="col-md-6">
                <p><b>Nº Indicencia: </b><?= $data['incidencia']->id ?>    //    <?= $data['incidencia']->solved === 'nr' ? 'Abierta el' : 'Solucionada el:' ?> <?= $data['incidencia']->created_at ?></p>
                
                <hr>
                
                <form class="needs-validation" action="<?= URLROOT.'/incidencias/update/'.isset($data['incidencia']->id) ? $data['incidencia']->id : $data['id']  ?>" method="post" enctype="multipart/form-data">
                    <p><b>Actualizar estado:</b></p>
                    <select name="solved" class="form-select mb-3" aria-label="Default select example">
                        <option <?= $data['incidencia']->solved === 'nr' ? 'selected' : ''?> value="nr">Sin resolver</option>
                        <option <?= $data['incidencia']->solved === 'r' ? 'selected' : ''?> value="r">Resuelta</option>
                    </select>
                    <p><b>Descripción:</b></p>
                    <input type="text" class="form-control <?= empty($data['description_err']) ? '' : 'is-invalid'; ?> " value="<?= isset($data['description']) ? $data['description'] : $data['incidencia']->description ?>" name="description">

                    <?php if (isset($data['description_err'])) : ?>
                        <div id=" validationServerUsernameFeedback" class="invalid-feedback">
                            <?= $data['description_err']; ?>
                        </div>
                    <?php endif ?>



                    <hr>
                    <p><b>Comentario:</b></p>
                    <input type="text" class="form-control <?= empty($data['comment_err']) ? '' : 'is-invalid'; ?>" value="<?= isset($data['comment']) ? $data['comment'] : $data['incidencia']->comment?>" name="comment">

                    <?php if (isset($data['comment_err'])) : ?>
                        <div id=" validationServerUsernameFeedback" class="invalid-feedback">
                            <?= $data['comment_err']; ?>
                        </div>
                    <?php endif ?>


                    <hr>
                    <label for="image" class="form-label">Imagen <small>(Opcional)</small></label>
                    <input type="file" class="form-control <?= empty($data['image_err']) ? '' : 'is-invalid'; ?>" name="image"  value="<?= isset($data['incidencia']->image) ? $data['incidencia']->image : ''?>">

                        <?php if (isset($data['image_err'])) : ?>
                            <div id=" validationServerUsernameFeedback" class="invalid-feedback">
                                <?= $data['image_err']; ?>
                            </div>
                         <?php endif ?>

                    <button type="submit" class="btn btn-success mt-3">
                        <i class="far fa-edit"></i> Confirmar edición
                    </button>
                </form>
                <a href="<?= URLROOT.'/incidencias/index'?>" class="btn btn-warning mt-3"><i class="fas fa-arrow-left"></i>

                    Cancelar</a>

            </div>
            <div class="col-md-6 d-flex justify-content-center">
            <img src="<?= !empty($data['incidencia']->image) ? URLROOT.'/public/images/'.$data['incidencia']->image : URLROOT.'/public/images/default.jpg'?>" alt="Imagen de la incidencia" width="400px" height="400px">
            </div>          
        </div>
            
    </div>

<?php require_once APPROOT . '/views/partials/footer.php'; ?>