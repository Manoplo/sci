<?php require_once APPROOT . '/views/partials/header.php'; ?>
<?php require_once APPROOT . '/views/partials/navbar.php'; ?>
        <div class="container container-incidencia">
            <div class="row">
                <div class="col-md-12">
                <h1>Gestión de incidencias</h1>
                <div class="row">
                    <div class="col-md-12 d-flex">
                        <form action="<?= URLROOT.'/incidencias/list'?>" method="post" class="mt-3 mb-4">
                            <p>Ordenadar por: </p>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" checked type="radio" name="radiovalue" id="inlineRadio1" value="id">
                                <label class="form-check-label" for="inlineRadio1">Id</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="radiovalue" id="inlineRadio2" value="class">
                                <label class="form-check-label" for="inlineRadio2">Aula</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="radiovalue" id="inlineRadio3" value="created_at">
                                <label class="form-check-label" for="inlineRadio3">Fecha de creación</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="radiovalue" id="inlineRadio4" value="solved">
                                <label class="form-check-label" for="inlineRadio4">Estado</label>
                            </div>
                            <select name="order" class="form-select mt-3" aria-label="Default select example">
                                <option value="ASC">Ascendente</option>
                                <option value="DESC">Descendente</option>
                            </select>
                            <input type="submit" class="btn btn-success mt-3" value="Filtrar">
                            <?php if($_SESSION['user_rol'] === 'admin' ): ?>
                            <button type="button" class="btn btn-danger mt-3" data-bs-toggle="modal" data-bs-target="#exampleModal"><i class="fas fa-recycle"></i>
                                Eliminar todas las incidencias resueltas
                            </button>
                            <?php endif; ?>
                        </form>
                        
                        
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Vista Previa</th>
                                <th>Descripción</th>
                                <th>Aula</th>
                                <th>Fecha Creación</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($data['incidencias'] as $incidencia): ?>
                                <tr>
                                    <td><?= $incidencia->id ?></td>
                                    <td><img src="<?= !empty($incidencia->image) ? URLROOT.'/public/images/'.$incidencia->image : URLROOT.'/public/images/default.jpg'?>" alt="Imagen de la incidencia" width="50px"></td>
                                    <td><?= $incidencia->description ?></td>
                                    <td><?= $incidencia->class ?></td>
                                    <td><?= $incidencia->created_at ?></td>
                                    <td><?= $incidencia->solved === 'nr' ? 'Activa' : 'Resuelta'  ?></td>
                                    <td>
                                    <a href="<?= URLROOT.'/incidencias/show/'.$incidencia->id ?>" class="btn btn-warning btn-sm d-flex align-items-between"><span class="material-icons">
                                        search
                                        </span>Ver</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            
                        </tbody>
                    </table>
            <a href="<?= URLROOT.'/incidencias/index/'?>" class="btn btn-warning"><i class="fas fa-arrow-left"></i>Volver</a>
        </div>

                </div>
            </div>
        </div>
        <!--- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">¿Eliminar?</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Esta acción es irreversible. ¿Está seguro de que desea eliminar todas las incidencias resueltas?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                <form action="<?= URLROOT.'/incidencias/destroy/'?>" method="post">                        
                        <button type="submit" class="btn btn-danger">
                        <i class="fas fa-recycle"></i> Confirmar eliminación
                        </button>
                </form>
            </div>
            </div>
        </div>
    </div>

        
<?php require_once APPROOT . '/views/partials/footer.php'; ?>
