<?php require_once APPROOT . '/views/partials/header.php'; ?>
<?php require_once APPROOT . '/views/partials/navbar.php'; ?>

<div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-md-6">
                <div class="index-container">
                    <img src="<?php echo URLROOT; ?>/public/images/undraw_add_notes_re_ln36.svg" alt="Logo" width="20%" class="img-fluid mb-4">
                    <h2 class="text-center">
                        Introduce una nueva incidencia:
                    </h2>
                    <form method="POST" action="<?=URLROOT.'/incidencias/create' ?>" enctype="multipart/form-data" class="row g-3 needs-validation w-100 mt-3" novalidate>
                        <div class="col-12">
                            <label for="description" class="form-label">Descripción <small>(una breve descripción de la incidencia)</small></label>
                            <input type="text" class="form-control <?= empty($data['description_err']) ? '' : 'is-invalid'; ?>" name="description"  value="<?= isset($data['description']) ? $data['description'] : ''; ?>" required>
                            
                            <?php if (isset($data['description_err'])) : ?>
                                <div id=" validationServerUsernameFeedback" class="invalid-feedback">
                                    <?= $data['description_err']; ?>
                                </div>
                             <?php endif ?>


                        </div>
                        <div class="col-12">
                            <label for="comment" class="form-label">Comentario <small>(Un comentario más amplio sobre la incidencia)</small></label>
                            <input type="text" class="form-control <?= empty($data['comment_err']) ? '' : 'is-invalid'; ?>" name="comment" value="<?= isset($data['comment']) ? $data['comment'] : ''; ?>" required>

                            <?php if (isset($data['comment_err'])) : ?>
                                <div id=" validationServerUsernameFeedback" class="invalid-feedback">
                                    <?= $data['comment_err']; ?>
                                </div>
                             <?php endif ?>
                            
                        </div>
                        <div class="col-12">
                            <label for="class" class="form-label">Aula</label>
                            <input type="number" class="form-control <?= empty($data['class_err']) ? '' : 'is-invalid'; ?>" name="class" value="" required>

                            <?php if (isset($data['class_err'])) : ?>
                                <div id=" validationServerUsernameFeedback" class="invalid-feedback">
                                    <?= $data['class_err']; ?>
                                </div>
                             <?php endif ?>
                            
                        </div>
                        <div class="col-12">
                            <label for="image" class="form-label">Imagen <small>(Opcional)</small></label>
                            <input type="file" class="form-control <?= empty($data['image_err']) ? '' : 'is-invalid'; ?>" name="image"  value="">

                            <?php if (isset($data['image_err'])) : ?>
                                <div id=" validationServerUsernameFeedback" class="invalid-feedback">
                                    <?= $data['image_err']; ?>
                                </div>
                             <?php endif ?>
                            
                        </div>
                        
                        <div class="col-12">
                            <button class="btn btn-primary w-100" type="submit">Enviar</button>
                            <a class="btn btn-warning w-100 mt-3" href="<?= URLROOT .'/incidencias/index'?>">Volver</a>
                        </div>
                        </form>
                        <div class="col-12">
                            
                        </div>
                </div>
            </div>
        </div>
    </div>


<?php require_once APPROOT . '/views/partials/footer.php'; ?>