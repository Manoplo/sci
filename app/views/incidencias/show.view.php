<?php require_once APPROOT . '/views/partials/header.php'; ?>
<?php require_once APPROOT . '/views/partials/navbar.php'; ?>

    <div class="container container-incidencia">
        <div class="row">
            <div class="col-md-6">
                <p><b>Nº Indicencia: </b><?= $data['incidencia']->id ?>    //    <?= $data['incidencia']->solved === 'nr' ? 'Abierta el' : 'Solucionada el:' ?> <?= $data['incidencia']->created_at ?></p>
                <hr>
                <p><b>Descripción:</b></p>
                <h2><?= $data['incidencia']->description ?> -  Aula <?= $data['incidencia']->class?></h2>
                <hr>
                <p><b>Abierta por:</b></p>
                <h3><?= $data['user']->name?></h3>
                <hr>
                <p><b>Comentario:</b></p>
                <h4><?= $data['incidencia']->comment ?></h4>
                <hr>
                <a href="<?= URLROOT.'/incidencias/index'?>" class="btn btn-warning"><i class="fas fa-arrow-left"></i>

                    Volver</a>

                <a href="<?= URLROOT.'/incidencias/print/'.$data['incidencia']->id?>" class="btn btn-danger"><i class="far fa-file-pdf"></i>

                    Convertir a PDF</a>

                <?php if($_SESSION['user_id'] === $data['incidencia']->user_id || $_SESSION['user_rol'] === 'admin' ): ?>
                <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#exampleModal"><i class="fas fa-trash"></i>
                     Eliminar
                </button>
                <?php endif; ?>

                <?php if($_SESSION['user_rol'] === 'admin'): ?>
                    <a href="<?= URLROOT.'/incidencias/update/'.$data['incidencia']->id ?>" class="btn btn-success"><i class="far fa-edit"></i>Editar</a>
                <?php endif; ?>

            </div>
            <div class="col-md-6 d-flex justify-content-center">
            <img src="<?= !empty($data['incidencia']->image) ? URLROOT.'/public/images/'.$data['incidencia']->image : URLROOT.'/public/images/default.jpg'?>" alt="Imagen de la incidencia" width="400px">
            </div>          
        </div>
            
    </div>
            <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">¿Eliminar?</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Esta acción es irreversible. ¿Está seguro de que desea eliminar la incidencia?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                <form action="<?= URLROOT.'/incidencias/delete/'.$data['incidencia']->id ?>" method="post">                        
                        <button type="submit" class="btn btn-danger">
                            <i class="fas fa-trash"></i> Confirmar eliminación
                        </button>
                </form>
            </div>
            </div>
        </div>
    </div>


<?php require_once APPROOT . '/views/partials/footer.php'; ?>