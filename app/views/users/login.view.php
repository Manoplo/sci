<?php require_once APPROOT . '/views/partials/header.php'; ?>
<div class="main-wrapper">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-md-6">
                <div class="index-container">
                    
                    <img src="<?php echo URLROOT; ?>/public/images/logo-cut.png" alt="Logo" width="20%" class="img-fluid mb-4">
                    <div class="flashes">
                        <?= (string) flash() ?>
                    </div>
                    <h2 class="text-center">
                        Introduce tus datos de log-in:
                    </h2>
                    <form method="POST" action="<?=URLROOT.'/users/login' ?>" class="row g-3 needs-validation w-100 mt-3" novalidate>
                        <div class="col-12">
                            <label for="validationCustom01" class="form-label">Email</label>
                            <input type="text" class="form-control <?= empty($data['email_err']) ? '' : 'is-invalid'; ?>" name="email" id="validationCustom01" value="" required>

                            <?php if (isset($data['email_err'])) : ?>
                                <div id=" validationServerUsernameFeedback" class="invalid-feedback">
                                    <?= $data['email_err']; ?>
                                </div>
                             <?php endif ?>
                            
                        </div>
                        <div class="col-12">
                            <label for="validationCustom01" class="form-label">Contraseña</label>
                            <input type="text" class="form-control <?= empty($data['password_err']) ? '' : 'is-invalid'?>" name="password" id="validationCustom01" value="" required>
                            
                            <?php if (isset($data['password_err'])) : ?>
                                <div id=" validationServerUsernameFeedback" class="invalid-feedback">
                                    <?= $data['password_err']; ?>
                                </div>
                             <?php endif ?>
                        </div>
                        
                        <div class="col-12">
                            <button class="btn btn-primary w-100" type="submit">Log-in</button>
                            <a class="btn btn-warning w-100 mt-3" href="<?= URLROOT .'/'?>">Volver</a>
                        </div>
                        </form>
                        <div class="col-12">
                            
                        </div>
                    
                        
                       
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once APPROOT . '/views/partials/footer.php'; ?>