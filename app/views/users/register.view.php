<?php require_once APPROOT . '/views/partials/header.php'; ?>
<div class="main-wrapper">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-md-6">
                <div class="index-container">
                    <img src="<?php echo URLROOT; ?>/public/images/logo-cut.png" alt="Logo" width="20%" class="img-fluid mb-4">
                    <h2 class="text-center">
                        Introduce tus datos de registro:
                    </h2>
                    <form method="POST" action="<?=URLROOT.'/users/register' ?>" class="row g-3 needs-validation w-100 mt-3" novalidate>
                        <div class="col-12">
                            <label for="name" class="form-label">Nombre</label>
                            <input type="text" class="form-control <?= empty($data['name_err']) ? '' : 'is-invalid'; ?>" name="name"  value="<?= isset($data['name']) ? $data['name'] : ''; ?>" required>
                            
                            <?php if (isset($data['name_err'])) : ?>
                                <div id=" validationServerUsernameFeedback" class="invalid-feedback">
                                    <?= $data['name_err']; ?>
                                </div>
                             <?php endif ?>


                        </div>
                        <div class="col-12">
                            <label for="email" class="form-label">Email</label>
                            <input type="text" class="form-control <?= empty($data['email_err']) ? '' : 'is-invalid'; ?>" name="email" value="<?= isset($data['email']) ? $data['email'] : ''; ?>" required>

                            <?php if (isset($data['email_err'])) : ?>
                                <div id=" validationServerUsernameFeedback" class="invalid-feedback">
                                    <?= $data['email_err']; ?>
                                </div>
                             <?php endif ?>
                            
                        </div>
                        <div class="col-12">
                            <label for="password" class="form-label">Password</label>
                            <input type="text" class="form-control <?= empty($data['password_err']) ? '' : 'is-invalid'; ?>" name="password" value="" required>

                            <?php if (isset($data['password_err'])) : ?>
                                <div id=" validationServerUsernameFeedback" class="invalid-feedback">
                                    <?= $data['password_err']; ?>
                                </div>
                             <?php endif ?>
                            
                        </div>
                        <div class="col-12">
                            <label for="confirm_password" class="form-label">Confirma el password</label>
                            <input type="text" class="form-control <?= empty($data['confirm_password_err']) ? '' : 'is-invalid'; ?>" name="confirm_password"  value="" required>

                            <?php if (isset($data['confirm_password_err'])) : ?>
                                <div id=" validationServerUsernameFeedback" class="invalid-feedback">
                                    <?= $data['confirm_password_err']; ?>
                                </div>
                             <?php endif ?>
                            
                        </div>
                        
                        <div class="col-12">
                            <button class="btn btn-primary w-100" type="submit">Enviar</button>
                            <a class="btn btn-warning w-100 mt-3" href="<?= URLROOT .'/'?>">Volver</a>
                        </div>
                        </form>
                        <div class="col-12">
                            
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once APPROOT . '/views/partials/footer.php'; ?>