<nav>
    <div class="container">
        <div class="row d-flex flex-direction-row">
            
            <div class="col-sm-3 mt-1 d-flex align-items-center">
                <img src="<?php echo URLROOT; ?>/public/images/logo-cut.png" alt="Logo" width="15%">
                <h1 class="text-white p-2">SCI</h1>
            </div>

            <div class="col-sm-3 mt-4 ms-auto d-flex justify-content-around">
                 <img class="avatar avatar-32 bg-light rounded-circle text-white p-2 mr-2"
                    src="<?= !empty($_SESSION['user_avatar']) ? URLROOT.'/public/images/profiles/'.$_SESSION['user_avatar'] : 'https://raw.githubusercontent.com/twbs/icons/main/icons/person-fill.svg'?>">

                <p class="text-white"><?= $_SESSION['user_name']?><span class="badge rounded-pill <?= $_SESSION['user_rol'] === 'visitor' ? 'bg-success' : ($_SESSION['user_rol'] === 'user' ? 'bg-danger' : 'bg-dark') ?> ml-2"><?= $_SESSION['user_rol'] === 'visitor' ? 'Visitante' : ($_SESSION['user_rol'] === 'user' ? 'Usuario' : 'Admin') ?></span></p>

                <a class="options" href="<?= URLROOT.'/profiles/edit/'.$_SESSION['user_id']?>"><i class="fas fa-cog options"></i></a>
                
            </div>
            <div class="col-sm-auto mt-3">
                <a href="<?= URLROOT.'/users/logout' ?>" class="btn btn-warning text-dark d-flex justify-content-center align-content-between"><span class="material-icons">logout</span>Logout</a>
            </div>
        </div>
        </div>
    </div>
</nav>