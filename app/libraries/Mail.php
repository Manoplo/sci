<?php

class Mail{

    private $adminEmail;

    public function __construct(){
        $this->adminEmail = 'test@mailhog.local';
    }

    /**
     * Function send
     * Sends a mail
     * @param [string] $to
     * @param [string] $subject
     * @param [string] $body
     * @return boolean
     */
    public function send($to, $subject, $body){

        $headers  = "From: no-reply <admin@adminsci.com>\n";
        $response = mail($to, $subject, $body, $headers);
        return $response;
    }
    /**
     * Function sendNewUser
     * Sends a mail when a user has registered into the system. 
     * @param [string] $user
     * @param [string] $email
     * @return boolean
     */
    public function sendNewUser($user, $email){
            
        $response =  $this->send($this->adminEmail, 'Nuevo usuario', 'Nuevo usuario registrado en el sistema: '.$user.' con email: '.$email. PHP_EOL);
        return $response;
    }
    

    /**
     * Function sendNewIncidencia
     * Sends a mail when a user has created a new incidence. 
     * @param [string] $user
     * @param [string] $description
     * @param [string] $comment
     * @return boolean
     */
    public function sendNewIncidencia($user, $description, $comment){
                
            $response =  $this->send($this->adminEmail, 'Nueva incidencia', "Nueva incidencia registrada por $user" . PHP_EOL . "Descripción: $description" . PHP_EOL . "Comentario: $comment");
            return $response;
    }

    /**
     * Function sendRoleChange
     * Sends a mail when a user´s role has been changed. 
     * @param [string] $email
     * @param [string] $rol
     * @return boolean
     */
    public function sendRoleChange($email, $rol){
                    
            $response =  $this->send($this->adminEmail, 'Cambio de rol', "Se ha cambiado el rol para el usuario con email $email. Su nuevo rol es $rol". PHP_EOL);
            return $response;
    }

}