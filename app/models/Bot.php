<?php

/**
 * Class Bot - Model
 */
class Bot{

    private $db;

    public function __construct(){
        $this->db = new Database;
    }
    /**
     * function getAnswer
     * Retrieves an answer from bot table. 
     * @param [string] $question
     * @return object || boolean
     */
    public function getAnswer($question){

        $this->db->query('SELECT * FROM bot WHERE question like :question');
        $question = "%$question%";
        $this->db->bind(':question', $question);
        
        $row = $this->db->getOneResult('Bot');

        if($row){
            return $row;
        } else {
            return false;
        }
    }
    /**
     * Function addQuestion
     * Inserts a register into bot table question field
     * @param [string] $question
     * @return boolean
     */
    public function addQuestion($question){

        $this->db->query('INSERT INTO bot (question) VALUES (:question)');
        $this->db->bind(':question', $question);
        $operation =  $this->db->execute();

        if($operation){
            return true;
        } else {
            return false;
        }
    }
    /**
     * Function getNullAnswers
     * Retrieves all null answers from bot table. 
     * @return void
     */
    public function getNullAnswers(){
            
            $this->db->query('SELECT * FROM bot WHERE answer IS NULL');
            $rows = $this->db->getAllResults('Bot');
    
            if($rows){
                return $rows;
            } else {
                return false;
            }

    }
    /**
     * Function updateAnswer
     * Updates an answer field. 
     * @param [string] $id
     * @param [string] $answer
     * @return boolean
     */
    public function updateAnswer($id, $answer){

        $this->db->query('UPDATE bot SET answer = :answer WHERE id = :id');
        $this->db->bind(':answer', $answer);
        $this->db->bind(':id', $id);
        $operation =  $this->db->execute();

        if($operation){
            return true;
        } else {
            return false;
        }
    }
}