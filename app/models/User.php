<?php
/**
 * Class User - model
 */
class User{

    private $db;

    public function __construct(){

        $this->db = new Database();
    }
    
    /**
     * Function getAllProfiles
     * Retrieves all users. 
     * @return array
     */
    public function getAllProfiles(){

        $this->db->query("SELECT * FROM users");
        $results = $this->db->getAllResults('User');
        return $results;
    }
    /**
     * Function searchProfiles
     * Search users meeting clients inputs. 
     * @param [string] $input
     * @return object
     */
    public function searchProfiles($input){

        $this->db->query('SELECT * FROM users WHERE name or email LIKE :input');
        $input = "%$input%";
        $this->db->bind(':input', $input);
        $results = $this->db->getAllResults('User');
        return $results;
    }

    /**
     * Function findUserByEmail
     * Retrieves an user by email input. 
     * @param [string] $email
     * @return boolean
     */
    public function findUserByEmail($email){

        $this->db->query("SELECT * FROM users where email like :email");
        $this->db->bind(':email', $email);
        
        $result = $this->db->getOneResult('User');

        if($result){
            return true;
        }else{
            return false;
        }

    }
    /**
     * Function getUserById
     * Retrieves a user by id. 
     * @param [string] $id
     * @return object || boolean
     */
    public function getUserById($id){

        $this->db->query("SELECT * FROM users where id = :id");
        $this->db->bind(':id', $id);
        
        $result = $this->db->getOneResult('User');
        
        if($result){
            return $result;
        }else{
            return false;
        }

    }

    /**
     * Function register
     * Register a user into user table. 
     * @param [array] $data
     * @return boolean
     */
    public function register($data){

        $this->db->query("INSERT into users(name, email, password) values(:name, :email, :password)");
        $this->db->bind(':name', $data['name']);
        $this->db->bind(':email', $data['email']);
        $this->db->bind(':password', password_hash($data['password'], PASSWORD_DEFAULT));
        $result = $this->db->execute();

        


        if ($result) {
            return true;
        } else {
            throw PDOException;
        }

    }
    /**
     * Function login
     * Checks if user exists.
     * @param [string] $email
     * @param [string] $password
     * @return object || boolean
     */
    public function login($email, $password){

        $this->db->query("SELECT * FROM users where email like :email");
        $this->db->bind(':email', $email);
        $result = $this->db->getOneResult('User');

        if(password_verify($password, $result->password)){

            return $result;
            
        }else{

            return false;
        }
    }
    /**
     * Function updateRol
     * Updates rol field in users table. 
     * @param [array] $data
     * @return boolean
     */
    public function updateRol($data){
        $this->db->query("UPDATE users set rol = :rol where id = :id");
        $this->db->bind(':rol', $data['rol']);
        $this->db->bind(':id', $data['id']);
        $result = $this->db->execute();

        if($result){
            return true;
        }else{
            throw new PDOException('Error updating rol');
        }
    }
    /**
     * Function updateUser
     * Updates a user in db. 
     * @param [array] $data
     * @return boolean
     */
   public function updateUser($data){
        $this->db->query("UPDATE users set name = :name, email = :email, image = :image where id = :id");
        $this->db->bind(':name', $data['name']);
        $this->db->bind(':email', $data['email']);
        $this->db->bind(':image', $data['image']);
        $this->db->bind(':id', $data['id']);
        $result = $this->db->execute();

        if($result){
            return true;
        }else{
            throw new PDOException('Error updating user');
        }
    }
    /**
     * Function deleteUser
     * Deletes a user by id. 
     * @param [string] $id
     * @return boolean
     */
    public function deleteUser($id){
        $this->db->query("DELETE from users where id = :id");
        $this->db->bind(':id', $id);
        $result = $this->db->execute();

        if($result){
            return true;
        }else{
            throw new PDOException('Error deleting user');
        }
    }
}

   
    