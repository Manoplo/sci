<?php

/**
 * Class Incidencia - Model
 */
class Incidencia{

    private $db;

    public function __construct(){
        $this->db = new Database;
    }

    /**
     * Function getAllIncidencias
     * Retrieves all from incidencias table. 
     * @return array
     */
    public function getAllIncidencias(){

        $this->db->query("SELECT * FROM incidencias");
        $results = $this->db->getAllResults('Incidencia');
        return $results;
    }
    /**
     * Function getIncidencias
     * Retrieves all incidencias according to solved field. 
     * @param [string] $str
     * @return array
     */
    public function getIncidencias($str){

        $this->db->query('SELECT * FROM incidencias WHERE solved = :str');

        $this->db->bind(':str', $str);
        $result = $this->db->getAllResults('Incidencia');
        return $result;
    }
    /**
     * Function getOrderedIncidencias
     * Retrieves all from incidencias field meeting values and orders filters. 
     * @param [string] $value
     * @param [string] $order
     * @return array
     */
    public function getOrderedIncidencias($value, $order){
            
            $this->db->query("SELECT * FROM incidencias ORDER BY $value $order");
            $results = $this->db->getAllResults('Incidencia');
            return $results;
    }
    /**
     * Function getIncidenciaById
     * Retrives a single incidencia register by id
     * @param [string] $id
     * @return object
     */
    public function getIncidenciaById($id){
            
            $this->db->query('SELECT * FROM incidencias WHERE id = :id');
            $this->db->bind(':id', $id);
            $result = $this->db->getOneResult('Incidencia');
            return $result;
    }
    /**
     * function createIncidencia
     * Creates a incidencia register
     * @param [array] $data
     * @return boolean
     */
    public function createIncidencia($data){

        $this->db->query('INSERT INTO incidencias (user_id, description, comment, class, image) VALUES (:user_id, :description, :comment, :class, :image)');
        $this->db->bind(':user_id', $data['user_id']);
        $this->db->bind(':description', $data['description']);
        $this->db->bind(':comment', $data['comment']);
        $this->db->bind(':class', $data['class']);
        $this->db->bind(':image', $data['image']);
        $result = $this->db->execute();

        if($result){
            return true;
        }else{
            return false;
        }
    }
    /**
     * Function deleteIncidencia
     * Deletes an incidencia register by id. 
     * @param [string] $id
     * @return boolean
     */
    public function deleteIncidencia($id){
        $this->db->query('DELETE FROM incidencias WHERE id = :id');
        $this->db->bind(':id', $id);
        $result = $this->db->execute();

        if($result){
            return true;
        }else{
            return false;
        }
    }
    /**
     * Function updateIncidencia
     * Updates an incidencia register. 
     * @param [array] $data
     * @return boolean
     */
    public function updateIncidencia($data){
        $this->db->query('UPDATE incidencias SET description = :description, comment = :comment, image = :image, solved = :solved WHERE id = :id');
        $this->db->bind(':solved', $data['solved']);
        $this->db->bind(':description', $data['description']);
        $this->db->bind(':comment', $data['comment']);
        $this->db->bind(':image', $data['image']);
        $this->db->bind(':id', $data['id']);
        $result = $this->db->execute();

        if($result){
            return true;
        }else{
            return false;
        }
    }
    /**
     * Function destroyAll
     * Delete all incidencia registers matching "r" in "solved" field. 
     * @return boolean
     */
    public function destroyAll(){
        
        $this->db->query("DELETE FROM incidencias WHERE solved = 'r'");
        $result = $this->db->execute();

        if($result){
            return true;
        }else{
            return false;
        }
    }
    
}