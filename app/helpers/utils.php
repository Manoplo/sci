<?php 
/**
 * Function vadAndDie
 * Dumps a variable and dies. 
 * @param [any] $var
 * @return void
 */
function varAndDie($var) {
   
    var_dump($var) and die();
    
}
/**
 * Function urlRedirect
 * Redirects the user to an specific url. 
 * @param [string] $url
 * @return void
 */
function urlRedirect($url){

    header('location:'.URLROOT.$url);

}
/**
 * Function isLoggedIn
 * Checks if user is logged. 
 * @return boolean
 */
function isLoggedIn() {
    
    if(isset($_SESSION['user_id'])){
        return true; 
    }else{
        return false;
    }
}
/**
 * Function isAdmin
 * Checks if user is admin. 
 * @return boolean
 */
function isAdmin() {
        
    if(isset($_SESSION['user_id']) && $_SESSION['user_rol'] === 'admin'){
        return true; 
    }else{
        return false;
    }
}
/**
 * Function getReport
 * Creates a basic HTML variable to print pdfs. 
 * @param [string] $id
 * @param [string] $description
 * @param [string] $comment
 * @param [string] $created_at
 * @param [string] $username
 * @param [string] $situation
 * @return string
 */
function getReport($id, $description, $comment, $created_at, $username, $situation){


    $html = '<p style="font-family: sans-serif; color: blue;">Sistema de Control de Incidencias. CIFP Majada Marcial.</p>'.
            "<h1 style='font-family: sans-serif; color: gray;'>Reporte de incidencia: $id </h1>".
            '<hr>'.
            '<h2 style="font-family: sans-serif">Descripción: '.$description.'</h2>'.
            '<h2 style="font-family: sans-serif">Comentario: '.$comment.'</h2>'.
            '<h2 style="font-family: sans-serif">Creada el: '.$created_at.'</h2>'.
            '<h2 style="font-family: sans-serif">Creada por: '.$username.'</h2>'.
            '<hr>'.
            '<h2 style="font-family: sans-serif">Situación: '.$situation.'</h2>';
            

    return $html;
}