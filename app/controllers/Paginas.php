<?php

class Paginas extends Controller

{
    public function __construct()
    {

        // Desde aquí cargaremos los modelos // ------------->


    }
    /**
     * Function index
     * Returns the entry point view. 
     * @return void
     */
    public function index()
    {

        $data = [
            'titulo' => 'Sistema de Control de Incidencias'
        ];

        return $this->view('paginas/index', $data);
    }
}
