<?php

use \Tamtamchik\SimpleFlash\Flash;

/**
 * Chatbot class inherits from libraries/Controller
 */
class Chatbot extends Controller{
    
    private $botModel;

    public function __construct(){

        
        $this->botModel = $this->model('Bot');
    }

    /**
     * Function chat
     * Insert a question into the Bot db. 
     * @return void
     */
    public function chat(){

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
           
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [

                'question' => trim($_POST['question']),
                'answer' => '',
                'question_err' => '',
                'no_answer' => ''
            ];

            if(empty($data['question'])){

                $data['question_err'] = '¿Me vas a preguntar algo?';
            }

            if(empty($data['question_err'])){
                    
                try {

                    $answer = $this->botModel->getAnswer($data['question']);
                    
                    if(!$answer){

                        $this->botModel->addQuestion($data['question']);
                        $data['no_answer'] = 'No tengo respuesta para eso. Pero la tendré...';
                       
                    }else{
                            
                        $data['answer'] = $answer;
                    }
                    

                } catch (\PDOException $err) {
                    echo $err->getMessage();

                }
      
            }
            $this->view('chatbot/chat', $data);

        }else{

            $data = [

                'question' => '...',
                'waiting' => 'Esperando tus preguntas me hayo...',
               
            ];

            $this->view('chatbot/chat', $data);
        }
        
    }

    /**
     * Function train
     * Get null answers from Bot table. 
     * @return void
     */
    public function train(){

        if(!isAdmin()){
            urlRedirect('/chatbot/chat');
        }


        try {

            $nullAnswers = $this->botModel->getNullAnswers();
            

        } catch (\PDOException $err) {

            echo $err->getMessage();
        }
        

        $data = [

            'nullAnswers' => $nullAnswers
        ];

        $this->view('chatbot/train', $data);
    }
    
    /**
     * Function learn
     * Inserts an answer into Bot table register. 
     * @param [string] $id
     * @return void
     */
    public function learn($id){

        if($_SERVER['REQUEST_METHOD'] === 'POST'){
           
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [

                'answer' => trim($_POST['answer']), 
                'id' => $id
            ];
                    
                try {
                    
                    $this->botModel->updateAnswer($data['id'], $data['answer'] );
                    $flash = new Flash();
                    $flash->success('Gracias por entrenarme, Papá.');
                    var_dump($data);
                    urlRedirect('/chatbot/train');

                } catch (\PDOException $err) {
                    echo $err->getMessage();

                }
      
        }else{

            $nullAnswers = $this->botModel->getNullAnswers();

            $data = [

                'nullAnswers' => $nullAnswers
            ];

            $this->view('chatbot/learn', $data);
        }
        

    }
}