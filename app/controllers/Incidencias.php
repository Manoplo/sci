<?php

use \Tamtamchik\SimpleFlash\Flash;
use Dompdf\Dompdf;

/**
 * Class Incidencias inherits from libraries/Controller
 */
class Incidencias extends Controller{


    private $incidenciaModel;
    private $userModel;


    public function __construct(){
        
        if(!isLoggedIn()){
            urlRedirect('/users/login');
        }


        $this->incidenciaModel = $this->model('Incidencia');
        $this->userModel = $this->model('User');
    }

    /**
     * Function index
     * Sends data to index view. 
     * @return void
     */
    public function index(){

           
             try {

                $incidenciasUnsolved = $this->incidenciaModel->getIncidencias('nr'); 
                $incidenciasSolved = $this->incidenciaModel->getIncidencias('r'); 
                
                
            } catch (\PDOException $e) {
                echo $e;
            } 
             
    
            $data = [

                
                'incidenciasU' => $incidenciasUnsolved,
                'incidenciasS' => $incidenciasSolved
            ];
    
            $this->view('incidencias/index', $data);

    }
    /**
     * Function create
     * Create a register into Incidencias table.
     * Sends data to create view. 
     * @return void
     */
    public function create(){
            
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
    
                // Sanitize POST array
                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    
                $data = [

                    'user_id' => $_SESSION['user_id'],
                    'description' => trim($_POST['description']),
                    'comment' => trim($_POST['comment']),
                    'class' => trim($_POST['class']),
                    'image' => !empty($_FILES) ? $_FILES['image']['name'] : '',
                    'description_err' => '',
                    'comment_err' => '',
                    'class_err' => '',
                    'comment_err' => '',
                    'image_err' => ''
                ];
    
                // Validate data
                if(empty($data['description'])){
                    $data['description_err'] = 'Por favor, introduzca una descripción';
                }
                if(strlen($data['description']) > 35){
                    $data['description_err'] = 'La descripción no puede sobrepasar los 20 caracteres';
                }
    
                if(empty($data['comment'])){
                    $data['comment_err'] = 'Por favor, introduzca un comentario';
                }
    
                if(empty($data['class'])){
                    $data['class_err'] = 'Por favor, introduzca un aula';
                }


                // Validación e inserción de imagen. 
                if(!empty($data['image'])){
                       

                    $arrayTypes = ['image/jpeg', 'image/png', 'image/gif'];
                    $fileArray = $_FILES['image'];
              
    
                    try {
    
                        $file = new File($fileArray, $arrayTypes );
                        $file->validate();
                        $file->saveUploadedFile('images/');
                        
                        
                    } catch (FileException $error) {
                        
                        $data['image_err'] = $error->getMessage();
                        
    
                    }
                }
    
                // Make sure no errors
                if(empty($data['description_err']) && empty($data['comment_err']) && empty($data['class_err']) && empty($data['image_err'])){
                    // Validated
                    
                    try {
                        
                        $this->incidenciaModel->createIncidencia($data);
                        $mail = new Mail();
                        $mail->sendNewIncidencia($_SESSION['user_name'], $data['description'], $data['comment']);
                        $flash = new Flash();
                        $flash->success('Incidencia creada con éxito. ¡Gracias!');
                        urlRedirect('/incidencias/index');
                        

                    } catch (\PDOException $error) {

                        echo $error;
                    }

                } else {
                    // Load view with errors
                    $this->view('incidencias/create', $data);
                }
    
            } else {

                $data = [

                    'description' => '',
                    'comment' => '',
                    'class' => '',
                    'image' => '',
                    'description_err' => '',
                    'comment_err' => '',
                    'class_err' => '',
                    'comment_err' => '',
                    'image_err' => ''
                ];
               
                $this->view('incidencias/create', $data);
            }
    
        }
        /**
         * Function show
         * Retrieves a single register and sends it to show view. 
         * @param [string] $id
         * @return void
         */
        public function show($id){
            
            try {

                $incidencia = $this->incidenciaModel->getIncidenciaById($id);
                $user = $this->userModel->getUserById($incidencia->user_id);
                
                
            } catch (\PDOException $e) {
                echo $e;
            } 
            
            $data = [

                'incidencia' => $incidencia,
                'user' => $user
                
            ];
            
            $this->view('incidencias/show', $data);

        }
        /**
         * Function delete
         * Deletes one register by id
         * @param [string] $id
         * @return void
         */
        public function delete($id){
                
                if($_SERVER['REQUEST_METHOD'] == 'POST'){
        
                    // Get existing post from model
                    $incidencia = $this->incidenciaModel->getIncidenciaById($id);
                    $path = 'images/'.$incidencia->image;
        
                    // Check for owner
                    if($incidencia->user_id !== $_SESSION['user_id'] && !isAdmin()){
                        $flash = new Flash();
                        $flash->danger('No tienes permiso para borrar esa incidencia');
                        urlRedirect('/incidencias/index');
                    }
        
                    try {
                        // Eliminar imagen asociada. 
                        if(file_exists($path)){
                            unlink($path);
                        }
                        $this->incidenciaModel->deleteIncidencia($id);
                        $flash = new Flash();
                        $flash->success('Incidencia eliminada con éxito. ¡Gracias!');
                        urlRedirect('/incidencias/index');
                        

                    } catch (PDOException $err) {
                        echo $err;
                    }
                   
        
                } else {
                    urlRedirect('/incidencias/index');
                }
        }

        /**
         * Function update
         * Updates a register by id
         * @param [string] $id
         * @return void
         */
        public function update($id){
            

            if($_SERVER['REQUEST_METHOD'] == 'POST'){

                

                // Sanitize POST array
                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    
                $data = [
                    'id' => $id,
                    'description' => trim($_POST['description']),
                    'comment' => trim($_POST['comment']),
                    'solved' => $_POST['solved'],
                    'image' => !empty($_FILES) ? $_FILES['image']['name'] : '',
                    'description_err' => '',
                    'comment_err' => '',
                    'image_err' => '',
                ];
    
                // Validate data
                if(empty($data['description'])){
                    $data['description_err'] = 'Por favor, introduzca una descripción';
                }
                if(strlen($data['description']) > 35){

                    $data['description_err'] = 'La descripción no puede sobrepasar los 35 caracteres';
                }

                if(empty($data['comment'])){

                    $data['comment_err'] = 'Por favor, introduzca un comentario';
                }


                if(!empty($data['image'])){
               
                    $arrayTypes = ['image/jpeg', 'image/png', 'image/gif'];
                    $fileArray = $_FILES['image'];
                    $incidencia = $this->incidenciaModel->getIncidenciaById($id);
                    $path = 'images/'.$incidencia->image;
                    try {

                        unlink($path);
                        $file = new File($fileArray, $arrayTypes );
                        $file->validate();
                        $file->saveUploadedFile('images/');
                        
                    } catch (FileException $error) {
                        
                        $data['image_err'] = $error->getMessage();
                        
    
                    }

                }else{
                    // Si no se cambia la imagen, se guarda la misma.
                    $incidencia = $this->incidenciaModel->getIncidenciaById($id);
                    $data['image'] = $incidencia->image;
                }

                // Make sure no errors

                if( empty($data['description_err']) && empty($data['comment_err']) && empty($data['class_err']) && empty($data['image_err']) ){
                    // Validated
                    try {
                        
                        $this->incidenciaModel->updateIncidencia($data, $id);
                        $flash = new Flash();
                        $flash->success('Incidencia actualizada con éxito. ¡Gracias!');
                        urlRedirect('/incidencias/index');
                        

                    } catch (\PDOException $error) {

                        echo $error;
                    }

                } else {

                    
                    $this->view('incidencias/update', $data);
                    
                }

            } else {

                $incidencia = $this->incidenciaModel->getIncidenciaById($id);
                $data = [

                    'incidencia' => $incidencia,
                ];
                $this->view('incidencias/update', $data);
            }
        }
        /**
         * Function list
         * Get registers filtered by order or field
         * @return void
         */
        public function list(){

            if($_SERVER['REQUEST_METHOD'] === 'POST'){
                // FETCH DATA FROM FORM TO ORDER BY VALUE AND ASCENDING OR DESCENDING

                // SANITIZE POST ARRAY
                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

                // GET VALUE FROM POST ARRAY
                $data = [

                    'value' => $_POST['radiovalue'],
                    'order' => $_POST['order'],
                ];

                // GET ALL INCIDENCIAS

                try {

                    $incidencias = $this->incidenciaModel->getOrderedIncidencias($data['value'], $data['order']);

                    $data = [
                        'incidencias' => $incidencias,
                    ];
    
                    $this->view('incidencias/list', $data);

                } catch (\PDOException $err) {
                    echo $err;
                }


            }else{

                $incidencias = $this->incidenciaModel->getAllIncidencias();
                $data = [
    
                    'incidencias' => $incidencias
                ];
                $this->view('incidencias/list', $data);
            }

        }

        /**
         * Function destroy
         * Delete all registers that meets a certain condition
         * @return void
         */
        public function destroy(){

            if($_SERVER['REQUEST_METHOD'] === 'POST'){

                $incidenciasR = $this->incidenciaModel->getIncidencias('r');
                
                try {
                    // Borrar las imágenes si no son default.jpg
                    foreach($incidenciasR as $incidencia){
    
                        if($incidencia->image !== 'default.jpg'){
                            $path = 'images/'.$incidencia->image;
                            unlink($path);
                        }
                    }
    
                    $this->incidenciaModel->destroyAll();
                    $flash = new Flash();
                    $flash->success('Todas las incidencias resueltas han sido eliminadas con éxito. ¡Gracias!');
                    urlRedirect('/incidencias/list');
                    
                } catch (\PDOException $err) {
                    echo $err;     
                }

            }else{
                
                urlRedirect('/incidencias/list');
            }


        }
        /**
         * Function print
         * Prints a register to pdf and save it in users browser/system. 
         * Needs DOMPdf dependency to make it work. 
         * Needs getReport() function in helpers/utils.php to make it work. 
         * @param [string] $id
         * @return void
         */
        public function print($id){

            $incidencia = $this->incidenciaModel->getIncidenciaById($id);
            $user = $this->userModel->getUserById($incidencia->user_id);


            $resolved = $incidencia->solved === 'nr' ? 'No resuelta' : 'Resuelta';
            $pdf = new Dompdf();
            $html = getReport($incidencia->id, $incidencia->description, $incidencia->comment, $incidencia->created_at, $user->name, $resolved);
                   
            $pdf->loadHtml($html);
            $pdf->render();
            $pdf->stream("Reporte$incidencia->id.pdf");

        }

    }