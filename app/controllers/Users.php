<?php

use \Tamtamchik\SimpleFlash\Flash;
/**
 * Class Users inherits from libraries/Controller
 */
class Users extends Controller {

    private $userModel;

    public function __construct() {

        
        $this->userModel = $this->model('User');
    }

  
    /**
     * Function register
     * Register a user into users table
     * @return void
     */
    public function register() {


        if( $_SERVER['REQUEST_METHOD'] === 'POST' ){

            // Sanitiza las entradas
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            
            // Capturamos datos
            $data = [

                'name' => $_POST['name'] ?? '',
                'email' => $_POST['email'] ?? '',
                'password' => $_POST['password'] ?? '',
                'confirm_password' => $_POST['confirm_password'] ?? '',
                'name_err' => '',
                'email_err' => '',
                'password_err' => '',
                'confirm_password_err' => '',

            ];
            // Trimeamos datos
            $data = array_map('trim', $data);

            // Comprobamos que no estén vacíos

            empty($data['name']) ? $data['name_err'] = 'El nombre es obligatorio' : '';
            empty($data['email']) ? $data['email_err'] = 'El email es obligatorio' : '';
            empty($data['password']) ? $data['password_err'] = 'La contraseña es obligatoria' : '';
            empty($data['confirm_password']) ? $data['confirm_password_err'] = 'La confirmación de la contraseña es obligatoria' : '';

        
            // VALIDACIONES

            // El email no es válido
            if (!empty($data['email']) && !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {

                $data['email_err'] = 'El email no es válido';
            }

            // Comprobar que el email del usuario no exista en la BBDD
            if (!empty($data['email']) && $this->userModel->findUserByEmail($data['email'])) {

                $data['email_err'] = 'El email ya existe en la base de datos';
            }

            // Las contraseñas coinciden

            if( !empty($data['password']) && !empty($data['confirm_password']) ){
                
                if( $data['password'] !== $data['confirm_password'] ){

                    $data['confirm_password_err'] = 'Las contraseñas no coinciden';
                }
            }

            // Al menos 6 caracteres y alfanumérica /*/*/CHECKAR

            if ( !empty($data['password']) && strlen($data['password']) < 6  ||  !empty($data['password']) && !preg_match('/\d/', $data['password'])) {

                $data['password_err'] = 'La contraseña debe tener al menos 6 caracteres y ha de contener números y letras';
            }

            // SI NO HAY ERRORES

            if (empty($data['name_err']) && empty($data['email_err']) && empty($data['password_err']) && empty($data['confirm_password_err'])){
                
                try {

                    $this->userModel->register($data);
                    $mail = new Mail();
                    $mail->sendNewUser($data['name'], $data['email']);
                    $flash = new Flash();
                    $flash->success('Usuario registrado correctamente. Ya puede loguear');
                    urlRedirect('/users/login');
                    
                } catch (\PDOException $err) {
                    echo $err;
                }


            }else{

                $this->view('users/register', $data);
            }

        }else{
                $data = [

                    'name' => '',
                    'email' =>  '',
                    'password' =>  '',
                    'confirm_password' => '',
                    'name_err' => '',
                    'email_err' => '',
                    'password_err' => '',
                    'confirm_password_err' => '',

                ];
                $this->view('users/register', $data);
            
        }
    }     

    

     
    /**
     * Function login
     * Test users login inputs against users db. Returns data to login view. 
     * @return void
     */
    public function login() {


        if( $_SERVER['REQUEST_METHOD'] === 'POST' ){
            

            // Sanitiza las entradas
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            
            // Capturamos datos
            $data = [

                'email' => $_POST['email'] ?? '',
                'password' => $_POST['password'] ?? '',
                'email_err' => '',
                'password_err' => '',

            ];
            // Trimeamos datos
            $data = array_map('trim', $data);

            // Comprobamos que no estén vacíos

            empty($data['email']) ? $data['email_err'] = 'El email es obligatorio' : '';
            empty($data['password']) ? $data['password_err'] = 'La contraseña es obligatoria' : '';

            // VALIDACIONES

            // El email no es válido
            if (!empty($data['email']) && !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {

                $data['email_err'] = 'El email no es válido';
            }

            // Comprobar que el email del usuario no exista en la BBDD
            if (!empty($data['email']) && !$this->userModel->findUserByEmail($data['email'])) {

                $data['email_err'] = 'No existe un usuario con ese email';
            }

            // Comprobar que la contraseña sea correcta


            // SI NO HAY ERRORES
            

            if (empty($data['email_err']) && empty($data['password_err'])){
                
                try {

                    $loggedInUser = $this->userModel->login($data['email'], $data['password']);

                    if($loggedInUser){

                        $this->createUserSession($loggedInUser);
                        urlRedirect('/incidencias/index');
                    }else{
                            
                            $data['password_err'] = 'Contraseña incorrecta';
                            $this->view('users/login', $data);
                    }
                    
                } catch (\PDOException $err) {
                    echo $err;

                }


            }else{

                $this->view('users/login', $data);
            }     

        }else{
                $data = [

                    'email' => '',
                    'password' =>  '',
                    'email_err' => '',
                    'password_err' => '',

                ];
                $this->view('users/login', $data);
            
        }
    }
    /**
     * Function createUSerSession
     * Creates $_SESSION superglobal variables to persist users data through navigation. 
     * @param [PDO Object] $user
     * @return void
     */
    public function createUserSession($user){

        $_SESSION['user_id'] = $user->id;
        $_SESSION['user_email'] = $user->email;
        $_SESSION['user_name'] = $user->name;
        $_SESSION['user_rol'] = $user->rol;
        $_SESSION['user_avatar'] = $user->image;
        

    }
    /**
     * Function logout
     * Logs a user out by unsetting $_SESSION variables. 
     * @return void
     */
    public function logout(){
            
            unset($_SESSION['user_id']);
            unset($_SESSION['user_email']);
            unset($_SESSION['user_name']);
            unset($_SESSION['user_rol']);
    
            session_destroy();
            urlRedirect('/users/login');
    }
}