<?php

use \Tamtamchik\SimpleFlash\Flash;
/**
 * Class Profiles inherits from Controller
 */
class Profiles extends Controller{

    
    private $userModel;
    private $incidenciaModel;

    public function __construct() {
        
        if(!isLoggedIn()){
            urlRedirect('/users/login');
        }
        
        $this->userModel = $this->model('User');
        $this->incidenciaModel = $this->model('Incidencia');


    }
    /**
     * Function list
     * Get all users from table and sends it to list view. 
     * @return void
     */
    public function list() {

        if(!isAdmin()){
            urlRedirect('/incidencias/index');
        }

        try {
            $profiles = $this->userModel->getAllProfiles();
            //code...
        } catch (\PDOException $err) {
            echo $err;
        }
        
        $data = [

            'profiles' => $profiles
        ];


        $this->view('profiles/list', $data);
    }

    /**
     * Function show
     * Retrieves a single user and sends data to show view. 
     * @param [string] $id
     * @return void
     */
    public function show($id) {

        try {

            $profile = $this->userModel->getUserById($id);
            
            
        } catch (\PDOException $e) {
            echo $e;
        } 
        
        $data = [

            'profile' => $profile,
            // TODO: 'incidencias' => $indicendias
            
        ];
        
        $this->view('profiles/show', $data);

    }

    /**
     * Search
     * Search into users table a match with clients input. 
     * @return void
     */
    public function search() {

        if($_SERVER['REQUEST_METHOD'] == 'POST'){

            
            $data['input'] = trim(htmlspecialchars($_POST['input'])) ?? '';
            empty($data['input']) ? $data['input_err'] = 'No has introducido ningún valor de búsqueda' : '';

            if(empty($data['input_err'])){

                try {

                    $profiles = $this->userModel->searchProfiles($data['input']);
                    if(empty($profiles)){

                        try {
                            $profiles = $this->userModel->getAllProfiles();
                            //code...
                        } catch (\PDOException $err) {
                            echo $err;
                        }
                        
                        $data = [
                
                            'profiles' => $profiles,
                            'search_err' => 'La búsqueda no arrojó resultados. Inténtelo de nuevo con una búsqueda más concreta.'
                        ];
                
                
                        $this->view('profiles/list', $data);

                        

                    }else{

                        $data = [

                            'profiles' => $profiles,
                        ];

                        $this->view('profiles/list', $data);
                        
                    }
                    
                } catch (\PDOException $e) {
                    echo $e;
                }
            }

        }else{
            try {
                $profiles = $this->userModel->getAllProfiles();
                //code...
            } catch (\PDOException $err) {
                echo $err;
            }
            
            $data = [
    
                'profiles' => $profiles
            ];
    
    
            $this->view('profiles/list', $data);
        }
    }
    /**
     * Function rol
     * Updates a users role into his/her register. 
     * @param [string] $id
     * @return void
     */
    public function rol($id){

            if($_SERVER['REQUEST_METHOD'] == 'POST'){

                $data['rol'] = trim(htmlspecialchars($_POST['rol'])) ?? '';
                $data['id'] = $id;
                $user = $this->userModel->getUserById($id);

                try {

                    
                    $this->userModel->updateRol($data);
                    $mail = new Mail();
                    $mail->sendRoleChange($user->email, $data['rol']);
                    $flash = new Flash();
                    $flash->success('El rol del usuario se ha cambiado correctamente');
                    urlRedirect('/profiles/list');

                } catch (\PDOException $err) {

                    echo $err;
            }

        }else{
            urlRedirect('/profiles/show/'.$id);
        }
    }
    /**
     * Function delete
     * Delete an user. 
     * @param [string] $id
     * @return void
     */
    public function delete($id){

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
        
            // Get existing user from model
            $user = $this->userModel->getUserById($id);
            $path = 'images/profiles/'.$user->image;

            // Check if is admin
            if(!isAdmin()){
                $flash = new Flash();
                $flash->danger('No tienes permiso para borrar esa incidencia');
                urlRedirect('/incidencias/index');
            }

            try {
                // Eliminar imagen asociada. 
                if(file_exists($path)){
                    unlink($path);
                }
                $this->userModel->deleteUser($id);
                $flash = new Flash();
                $flash->success('Usuario eliminado con éxito. ¡Gracias!');
                urlRedirect('/profiles/list');
                

            } catch (PDOException $err) {
                echo $err;
            }          
     


        } else {
            urlRedirect('/profiles/list');
        }
    }

   
    /**
     * Function edit
     * Edits a user by id
     * @param [string] $id
     * @return void
     */
    public function edit($id){

       

        // Si el id de sesion de usuario no coincide con el id pasado por el link, redireccionar a la lista de incidencias. 
        if($_SESSION['user_id'] !== $id){
                
            urlRedirect('/profiles/list');
        }   

        if($_SERVER['REQUEST_METHOD'] == 'POST'){

            

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'id' => $id,
                'name' => trim($_POST['name']),
                'email' => trim($_POST['email']),
                'image' => !empty($_FILES['image']['name']) ? $_FILES['image']['name'] : '',
                'name_err' => '',
                'email_err' => '',
                'image_err' => ''
                
            ];
            


            if(empty($data['name'])){
                $data['name_err'] = 'Por favor, introduce un nombre';
            }

            if(empty($data['email'])){
                $data['email_err'] = 'Por favor, introduce un email';
            }

            if(!empty($data['image'])){
               
                $arrayTypes = ['image/jpeg', 'image/png', 'image/gif'];
                $fileArray = $_FILES['image'];
                $user = $this->userModel->getUserById($id);
                $path = 'images/profiles/'.$user->image;
                try {

                    unlink($path);
                    $file = new File($fileArray, $arrayTypes );
                    $file->validate();
                    $file->saveUploadedFile('images/profiles/');
                    
                } catch (FileException $error) {
                    
                    $data['image_err'] = $error->getMessage();
                    

                }

            }else{
                // Si no se cambia la imagen, se guarda la misma.
                $user = $this->userModel->getUserById($id);
                $data['image'] = $user->image;
            }

            if(empty($data['name_err']) && empty($data['email_err']) && empty($data['image_err'])){

                try {
                    $this->userModel->updateUser($data);
                    $flash = new Flash();
                    $flash->success('El usuario se ha actualizado correctamente');
                    urlRedirect('/profiles/list');
                } catch (\PDOException $err) {
                    echo $err;
                }
            }else{
                
                $this->view('profiles/edit', $data);
            }

        }else{
            $profile = $this->userModel->getUserById($id);
            $data = [
                
                'profile' => $profile,
            ];

            $this->view('profiles/edit', $data);
        }
    }

}